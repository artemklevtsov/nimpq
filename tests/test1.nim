import unittest
import os
import osproc
import strformat
import nimpq
import oids
import random

randomize()

proc genData(f: File, n: Natural) =
  f.write(""""id","value_1","value"""" & '\n')
  for i in 0 .. n:
    f.write('\"' & $genOid() & '\"' & ',')
    f.write('\"' & $rand(100) & '\"' & ',')
    f.write('\"' & $rand(1.0) & '\"' & '\n')

when isMainModule:

  block:
    const n = 10_000_000
    const fname = "ex-in.csv"
    var f: File
    if f.open(fname, fmWrite):
      f.genData(n)
      f.close()

  block:
    let dockerExe = findExe("docker")
    if dockerExe.len == 0:
      quit("Can't find docker execute binary.", QuitFailure)
    const port = 5432
    const dockerImg = "postgres:13-alpine"
    const dockerArgs = &"--detach --env POSTGRES_HOST_AUTH_METHOD=trust --publish {port}:{port}"
    const cmdOpts = {poEchoCmd, poEvalCommand}
    echo "Start docker container"
    let ps = execCmdEx(&"{dockerExe} run {dockerArgs} {dockerImg}", options = cmdOpts)
    if ps.exitCode != 0:
      quit("Can't start docker container.", QuitFailure)
    defer: discard execCmdEx(&"{dockerExe} stop {ps.output}", options = cmdOpts)
    echo "Wait for database"
    sleep(3000)
    echo "Connect to database"
    let db = open(conninfo = "postgresql://postgres@localhost:5432/postgres")
    #let db = open({"host": "127.0.0.1", "port": "5432", "user": "postgres", "dbname": "postgres"})
    defer: db.close()
    echo "Create table"
    db.exec("CREATE TABLE test (id text, value_1 bigint, value_2 double precision)")
    echo "Insert values"
    db.exec("INSERT INTO test(id, value_1, value_2) VALUES('602e73d0b602645e04bbb22f', 153, 15.0)")
    echo "Write to STDOUT"
    db.writeRows("SELECT * FROM test", stdout)
    echo "Cpoy to STDOUT"
    db.copyTo("COPY (SELECT * FROM test) TO STDOUT", stdout)
    db.copyTo("COPY (SELECT * FROM test) TO STDOUT", stdout)
    echo "Write to STDOUT"
    db.writeRows("SELECT * FROM test", stdout)
    echo "Copy from file"
    var fin: File
    if fin.open("ex-in.csv", fmRead):
      db.copyFrom("COPY test FROM STDIN (FORMAT csv, HEADER true)", fin)
      fin.close()
    echo "Copy to file"
    var fout: File
    if fout.open("ex-out.csv", fmWrite):
      db.copyTo("COPY (SELECT * FROM test) TO STDOUT (FORMAT csv, HEADER true, FORCE_QUOTE *)", fout)
      fout.close()
