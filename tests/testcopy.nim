import std/unittest
import std/os
import std/strutils
import std/strformat
import nimpq

include dbparams

let runTests = getEnv("CI") == "true"

suite "Test 'COPY FROM'":
  setup:
    let db = open(conninfo = dburl)
    discard db.exec(sql"SET client_min_messages TO WARNING")
    discard db.exec(sql"DROP TABLE IF EXISTS users")
    discard db.exec(sql"CREATE TABLE users(id serial, login text, balance numeric)")
    const fname = getTempDir() / "pgcopyfrom.csv"
    var f: File
    if f.open(fname, fmWrite):
      defer: f.close()
      f.write("user,login,balance\n")
      f.write("1,user1,0.0\n")
      f.write("2,user2,1.1\n")
      f.write("2,user3,2.2\n")

  teardown:
    removeFile(fname)
    discard db.exec(sql"DROP TABLE users")
    db.close()

  test "Copy to database":
    if f.open(fname, fmRead):
      defer: f.close()
      db.copyFrom(sql"COPY users FROM STDIN (FORMAT csv, HEADER true)", infile = f)
      let val = db.getValue(sql"SELECT count(*) FROM users").parseInt
      check val == 3

suite "Test 'COPY TO'":
  setup:
    let db = open(conninfo = dburl)
    discard db.exec(sql"SET client_min_messages TO WARNING")
    discard db.exec(sql"DROP TABLE IF EXISTS users")
    discard db.exec(sql"CREATE TABLE users(id serial, login text, balance numeric)")
    discard db.exec(sql"INSERT INTO users(id, login, balance) VALUES (1, 'user1', 0.0)")
    discard db.exec(sql"INSERT INTO users(id, login, balance) VALUES (2, 'user2', 1.1)")
    discard db.exec(sql"INSERT INTO users(id, login, balance) VALUES (3, 'user3', 2.2)")
    const fname = getTempDir() / "pgcopyto.csv"
    var f: File

  teardown:
    removeFile(fname)
    discard db.exec(sql"DROP TABLE users")
    db.close()

  test "Copy from database":
    if f.open(fname, fmWrite):
      defer: f.close()
      db.copyTo(sql"COPY (SELECT * FROM users) TO STDOUT (FORMAT csv, HEADER true, FORCE_QUOTE *)", outfile = f)
    let val = readFile(fname)
    check val.count('\n') == 4
    check val.count(',')  == 8
