#!/bin/sh

docker run --rm \
           --env POSTGRES_USER="test" \
           --env POSTGRES_PASSWORD="123456" \
           --env POSTGRES_DB="testdb" \
           --publish "5432:5432" \
           postgres:13-alpine
