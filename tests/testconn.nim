import std/unittest
import std/strformat
import std/os
import nimpq

include dbparams

let runTests = getEnv("CI") == "true"

suite "Test database connection":
  test "Connection error":
    const s = "postgresql://unknown:123456@unknown:99999/notexists"
    expect DbError:
      discard open(conninfo = s)
  test "Test connection string":
    const s = &"host={dbhost} port={dbport} user={dbuser} password='{dbpwd}' dbname={dbname}"
    let db = open(conninfo = s)
    defer: db.close()
    check db is DbConn
  test "Test connection URL":
    const s = &"postgresql://{dbuser}:{dbpwd}@{dbhost}:{dbport}/{dbname}"
    let db = open(conninfo = s)
    defer: db.close()
    check db is DbConn
  test "Test connection params":
    const p = {"host": dbhost, "port": $dbport, "user": dbuser, "password": dbpwd, "dbname": dbname}
    let db = open(dbparams = p)
    defer: db.close()
    check db is DbConn
  test "Test connection environment variables":
    putEnv("PGHOST", dbhost)
    putEnv("PGPORT", $dbport)
    putEnv("PGUSER", dbuser)
    putEnv("PGPASSWORD", dbpwd)
    putEnv("PGDATABASE", dbname)
    let db = open(conninfo = "")
    defer: db.close()
    check db is DbConn
