import private/wrapper
import std/strutils
import common

proc affcted(res: DBResult): int64 =
  let ans = $pqcmdTuples(res)
  result = if ans == "": 0 else: parseInt(ans)

proc value0(res: DBResult): string =
  if pqntuples(res) > 0:
    var ans = pqgetvalue(res, 0, 0)
    result = if isNil(ans): "" else: $ans
  else:
    result = ""

proc execQuery(db: DbConn, query: SqlQuery, args: varargs[string, `$`]): DBResult =
  if args.len > 0:
    var arr = allocCStringArray(args)
    result = pqexecParams(db, query.cstring, args.len.cint, nil, cast[ptr cstring](arr), nil, nil, 0)
    deallocCStringArray(arr)
  else:
    result = pqexec(db, query.cstring)

proc getEncoding*(db: DBConn): string =
  result = $pgEncodingToChar(pqclientEncoding(db))

proc setEncoding*(db: DbConn, encoding: string): bool =
  result = pqsetClientEncoding(db, encoding) == 0

proc prepare*(db: DbConn, stmtName: string, query: SqlQuery, nParams: int): SqlPrepared =
  if nParams > 0 and not string(query).contains("$1"):
    dbError("Parameter substitution expects \"$1\"")
  let res = pqprepare(db, stmtName, query.cstring, nParams.cint, nil)
  if pqResultStatus(res) != PGRES_COMMAND_OK:
    dbError(db)
  result = SqlPrepared(stmtName)

proc exec*(db: DbConn, query: SqlQuery, args: varargs[string, `$`]): int64 =
  let res = execQuery(db, query, args)
  if pqresultStatus(res) != PGRES_COMMAND_OK:
    dbError(db)
  result = affcted(res)
  pqclear(res)

proc exec*(db: DbConn, stmtName: SqlPrepared, args: varargs[string, `$`]): int64 =
  var arr = allocCStringArray(args)
  let res = pqexecPrepared(db, stmtName.cstring, args.len.cint, cast[ptr cstring](arr), nil, nil, 0)
  deallocCStringArray(arr)
  if pqresultStatus(res) != PGRES_COMMAND_OK:
    dbError(db)
  result = affcted(res)
  pqclear(res)

proc getValue*(db: DbConn, query: SqlQuery, args: varargs[string, `$`]): string =
  let res = execQuery(db, query, args)
  if pqresultStatus(res) != PGRES_TUPLES_OK:
    dbError(db)
  result = value0(res)
  pqclear(res)
