import private/wrapper
import common

template writeField(f: File, s: string | cstring): untyped =
  when s is cstring:
    let skip = isNil(s):
  elif s is string:
    let skip = s.len == 0
  if not skip:
    const q = '\"'
    f.write(q)
    for c in items(s):
      # double quote (RFC 4180 2.7)
      if c == q:
        f.write(q)
      f.write(c)
    f.write(q)

template writeSep(f: File, cond: untyped, sep, sep2: char): untyped =
  f.write(if cond: sep else: sep2)

proc sendQuery(db: DbConn, query: SqlQuery, args: varargs[string, `$`]) =
  if args.len > 0:
    var arr = allocCStringArray(args)
    if pqsendQueryParams(db, query.cstring, args.len.cint, nil, cast[ptr cstring](arr), nil, nil, 0) != 1:
      dbError(db)
    deallocCStringArray(arr)
  else:
    if pqsendQuery(db, query.cstring) != 1:
      dbError(db)
  if pqsetSingleRowMode(db) != 1:
    dbError(db)

iterator rows*(db: DbConn, query: SqlQuery, args: varargs[string, `$`]): seq[string] =
  db.sendQuery(query, args)
  var res: DBResult
  while true:
    res = pqgetResult(db)
    if isNil(res):
      break
    if pqresultStatus(res) == PGRES_TUPLES_OK:
      pqclear(res)
      continue
    if pqresultStatus(res) != PGRES_SINGLE_TUPLE:
      dbError(db)
    let ncols = pqnfields(res)
    let nrows = pqntuples(res)
    assert nrows == 1
    var ans = newSeqOfCap[string](ncols)
    for col in 0 ..< ncols:
      ans.add($pqgetvalue(res, 0, col))
    yield ans
    pqclear(res)

proc writeRows*(db: DbConn, query: SqlQuery, outfile: File = stdout, header: bool = false, sep = ',', args: varargs[string, `$`]) =
  ## Fetch rows from the database and write it to outfile.
  db.sendQuery(query, args)
  const eol = '\n'
  var res: DBResult
  var i: int64 = 0
  while true:
    res = pqgetResult(db)
    if isNil(res):
      break
    if pqresultStatus(res) == PGRES_TUPLES_OK:
      pqclear(res)
      continue
    if pqresultStatus(res) != PGRES_SINGLE_TUPLE:
      dbError(db)
    let ncols = pqnfields(res)
    let nrows = pqntuples(res)
    assert nrows == 1
    if header and i == 0:
      for col in 0 ..< ncols:
        outfile.writeField(pqfname(res, col))
        outfile.writeSep(col < ncols, sep, eol)
    for col in 0 ..< ncols:
      outfile.writeField(pqgetvalue(res, 0, col))
      outfile.writeSep(col < ncols - 1, sep, eol)
    inc(i)
    pqclear(res)
