import private/wrapper
import common

proc clearResult(db: DbConn) =
  var res: DBResult
  while true:
    res = pqgetresult(db)
    if isNil(res):
      break
    if pqresultStatus(res) != PGRES_COMMAND_OK:
      dbError(db)
    pqclear(res)

proc sendData(db: DBConn, outfile: File) =
  var buffer: cstring
  while true:
    let nbytes = pqgetcopydata(db, addr buffer, false.cint)
    if nbytes == -1: break
    if outfile.writeBuffer(addr buffer[0], nbytes) != nbytes:
      raise newException(IOError, "Can not write " & $nbytes & " to file.")
    pqfreemem(buffer)
  clearResult(db)

proc rcvData(db: DbConn, infile: File) =
  const bufsize = 8192
  var buffer = newString(bufsize)
  while true:
    let nbytes = infile.readBuffer(addr buffer[0], bufsize)
    if nbytes == 0: break
    if pqputcopydata(db, buffer, nbytes.cint) != 1:
      dbError(db)
  if pqputcopyend(db, nil) != 1:
    dbError(db)
  clearResult(db)

proc copyTo*(db: DbConn, query: SqlQuery, outfile: File = stdout) =
  let res = pqexec(db, query.cstring)
  if pqresultStatus(res) != PGRES_COPY_OUT:
    dbError(db)
  pqclear(res)
  sendData(db, outfile)

proc copyTo*(db: DbConn, query: SqlQuery, outfile: File = stdout, args: openArray[string]) =
  var arr = allocCStringArray(args)
  let res = pqexecParams(db, query.cstring, args.len.cint, nil, cast[ptr cstring](arr), nil, nil, 0)
  deallocCStringArray(arr)
  if pqresultStatus(res) != PGRES_COPY_OUT:
    dbError(db)
  pqclear(res)
  sendData(db, outfile)

proc copyFrom*(db: DbConn, query: SqlQuery, infile: File = stdin) =
  let res = pqexec(db, query.cstring)
  if pqresultStatus(res) != PGRES_COPY_IN:
    dbError(db)
  pqclear(res)
  rcvData(db, infile)

proc copyFrom*(db: DbConn, query: SqlQuery, infile: File = stdin, args: openArray[string]) =
  var arr = allocCStringArray(args)
  let res = pqexecParams(db, query.cstring, args.len.cint, nil, cast[ptr cstring](arr), nil, nil, 0)
  deallocCStringArray(arr)
  if pqresultStatus(res) != PGRES_COPY_IN:
    dbError(db)
  pqclear(res)
  rcvData(db, infile)
