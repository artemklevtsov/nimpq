import std/sequtils
import std/strutils
import private/wrapper

type
  DbConn* = ptr PGconn
  DBResult* = ptr PGResult
  DbError* = object of IOError
  SqlQuery* = distinct string
  SqlPrepared* = distinct string
  DBConnParam* = tuple
    key: string
    value: string

func `$`*(params: openArray[DBConnParam]): string =
  result = params.mapIt(it.key & "=" & it.value).join(" ")

proc dbError*(db: DbConn) =
  ## Raises a DbError exception.
  let msg = $pqErrorMessage(db)
  if msg.len > 0:
    raise newException(DbError, msg)

proc dbError*(db: DbConn, status: ConnStatusType) =
  if pqstatus(db) != status:
    dbError(db)

proc dbError*(res: DBResult) =
  let msg = $pqResultErrorMessage(res)
  if msg.len > 0:
    raise newException(DbError, msg)

proc dbError*(res: DBResult, status: ExecStatusType) =
  if pqresultStatus(res) != status:
    dbError(res)

proc dbError*(msg: string) =
  ## Raises a DbError exception.
  if msg.len > 0:
    raise newException(DbError, msg)

proc sql*(query: string): SqlQuery =
  SqlQuery(query)
