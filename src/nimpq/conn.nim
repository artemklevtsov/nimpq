import std/os
import private/wrapper
import common

proc open*(conninfo: string): DBConn =
  putEnv("LC_MESSAGES", "en_US.utf8")
  putEnv("PGCLIENTENCODING", "UTF-8")
  putEnv("PGTZ", "UTC")
  putEnv("PGDATESTYLE", "ISO")
  result = pqconnectdb(conninfo)
  if pqStatus(result) != CONNECTION_OK:
    dbError(result)

proc open*(dbparams: varargs[DBConnParam]): DbConn =
  result = open($dbparams)

proc close*(db: DbConn) =
  if not isNil(db):
    pqfinish(db)

proc ping*(conninfo: string): bool =
  let res = pqping(conninfo)
  if res == PQPING_NO_ATTEMPT:
    raise newException(DbError, "Error ping database (client side).")
  result = res == PQPING_OK

proc ping*(dbparams: varargs[DBConnParam]): bool =
  result = ping($dbparams)

proc traceOn*(db: DbConn, outfile: File = stdout) =
  pqtrace(db, outfile)

proc traceoff*(db: DbConn) =
  pquntrace(db)
