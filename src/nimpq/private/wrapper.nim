import std/os
import nimterop/build
import nimterop/cimport

static:
  #cDebug()
  let oldLang = getEnv("LANG")
  putEnv("LANG", "C")
  # use system path to search header
  cAddStdDir()
  putEnv("LANG", oldLang)

# shred library file
when defined(windows):
  const dllName = "libpq.dll"
elif defined(macosx):
  const dllName = "libpq.dylib"
else:
  const dllName = "libpq.so"

# header file
const hdrName = cSearchPath("libpq-fe.h")

cPlugin:
  import strutils
  proc onSymbol*(sym: var Symbol) {.exportc, dynlib.} =
    sym.name = sym.name.strip(chars={'_'}).replace("__", "_")
    if sym.kind in [nskProc, nskFunc, nskMethod]:
      if sym.name.startsWith("PQ"):
        sym.name = sym.name.replace("PQ", "pq")

cOverride:
  type
    pg_conn = object
    pg_result = object
    pg_cancel = object
    pg_int64 = int64
    Oid = int32

cImport(hdrName, false, dllName)
