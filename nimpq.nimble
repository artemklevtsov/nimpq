# Package

version       = "0.0.1"
author        = "Artem Klevtsov"
description   = "Nim libpq wrapper."
license       = "MIT"
srcDir        = "src"
backend = "cpp"

# Dependencies

requires "nim >= 1.4.2"
requires "nimterop 0.6.13"
